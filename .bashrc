#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
export EDITOR=vim

powerline-daemon -q
POWERLINE_BASH_CONTINUATION=1
POWERLINE_BASH_SELECT=1
. /usr/share/powerline/bindings/bash/powerline.sh

lock() {
    betterlockscreen -l -t "Don't touch those keys bitch!"
}


alias config='/usr/bin/git --git-dir=/home/rippee/.dotfiles/ --work-tree=/home/rippee'
